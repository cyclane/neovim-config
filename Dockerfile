FROM alpine

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk update

RUN mkdir -p /root/.config /root/.local/share/nvim/site/pack/packer/start
COPY . /root/.config/nvim
RUN mv /root/.config/nvim/init.lua /root/.config/nvim/init.norm.lua
RUN mv /root/.config/nvim/init.first.lua /root/.config/nvim/init.lua

RUN apk add neovim nodejs npm git curl g++ libgcc libc-dev python3 python3-dev py3-virtualenv py3-pip libstdc++
RUN git clone --depth 1 https://github.com/wbthomason/packer.nvim /root/.local/share/nvim/site/pack/packer/start/packer.nvim
RUN nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'
RUN mv /root/.config/nvim/init.norm.lua /root/.config/nvim/init.lua
RUN nvim --headless +'TSInstallSync all' +qall
WORKDIR /root/.local/share/nvim/site/pack/packer/start/coq_nvim
RUN python3 -m coq deps

RUN npm i -g graphql-language-service-cli vscode-langservers-extracted svelte-language-server typescript typescript-language-server
RUN pip3 install setuptools 'python-language-server[all]'

WORKDIR /root/project
CMD nvim
