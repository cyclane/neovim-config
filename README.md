# My [Neovim](https://neovim.io/) config
### Why separate from dotfiles?
- I use neovim on more systems I use the dotfiles on and therefore prefer for the config to have its own repo.
- (currently) I have stopped configuring any of the other dotfiles since I switched to KDE Plasma on OpenSUSE Tumbleweed.

### How to install
1. Copy the contents of this repository into your Neovim config folder.
2. [Install Packer.nvim](https://github.com/wbthomason/packer.nvim#quickstart).
3. Start neovim and press enter to ignore all the errors.
4. Run `:PackerInstall` within Neovim.
5. Restart Neovim and you should be good to go.

## Config files
### init.lua
The entry point for Neovim

### lua/plugins.lua
Contains all the plugins that this Neovim config uses.

### lua/settings.lua
Contains Neovim settings and (most) plugin settings.

### lua/lsp.lua
Contains LSP-related settings and LSP-related plugin settings.

### lua/mappings.lua
Contains (most) keyboard mappings.
