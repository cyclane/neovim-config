vim.g.coq_settings = {
	auto_start = true,
}

local lsp = require("lspconfig")
local coq = require("coq")

local on_attach = function(client, bufnr)
	local wk = require("which-key")

	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	local opts = { noremap=true, silent=true }

	wk.register({
		l = {
			name = "LSP",
			D = { function() vim.lsp.buf.declaration() end, "Declaration" },
			-- d = { function() vim.lsp.buf.definition() end, "Definition" },
			h = { function() vim.lsp.buf.hover() end, "Hover" },
			-- i = { function() vim.lsp.buf.implementation() end, "Implementation" },
			I = { function() vim.lsp.buf.signature_help() end, "Signature help" },
			k = {
				name = "Workspace",
				a = { function() vim.lsp.buf.add_workspace_folder() end, "Add folder" },
				r = { function() vim.lsp.buf.remove_workspace_folder() end, "Remove folder" },
				l = { function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, "List folders" }
			},
			-- t = { function() vim.lsp.buf.type_definition() end, "Type definition" },
			r = { function() vim.lsp.buf.rename() end, "Rename" },
			-- a = { function() vim.lsp.buf.code_action() end, "Code action" },
			-- R = { function() vim.lsp.buf.references() end, "References" },
			-- w = { function() vim.diagnostic.open_float() end, "Diagnostics (warnings)" },
			p = { function() vim.diagnostic.goto_prev() end, "Previous diagnostic" },
			n = { function() vim.diagnostic.goto_next() end, "Next diagnostic" },
			W = { function() vim.diagnostic.setloclist() end, "Diagnostics (warnings) list"},
			f = { function() vim.lsp.buf.formatting() end, "Formatting" },

			R = { "<cmd>Telescope lsp_references<CR>", "References" },
			g = { "<cmd>Telescope diagnostics<CR>", "Global diagnostics" },
			w = { "<cmd>Telescope diagnostics bufnr=0<CR>", "Diagnostics" },
			s = { "<cmd>Telescope lsp_document_symbols<CR>", "Document symbols" },
			S = { "<cmd>Telescope lsp_workspace_symbols<CR>", "Workspace symbols" },
			G = { "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>", "Dynamic workspace symbols" },
			a = { "<cmd>Telescope lsp_code_actions<CR>", "Code actions" },
			A = { "<cmd>Telescope lsp_range_code_actions<CR>", "Code actions for range" },
			i = { "<cmd>Telescope lsp_implementations<CR>", "Implementations" },
			d = { "<cmd>Telescope lsp_definitions<CR>", "Definitions" },
			t = { "<cmd>Telescope lsp_type_definitions<CR>", "Type definitions" }
		},
		f = {
			name = "File",
			s = {
				function()
					vim.lsp.buf.formatting_sync()
					vim.cmd [[ write ]]
				end, "Save"
			}
		}
	} , {
		prefix = "<leader>",
		buffer = bufnr
	})
end

local servers = { "graphql", "html", "eslint", "jsonls", "cssls", "svelte", "tsserver" }
for _, server in ipairs(servers) do
	lsp[server].setup(coq.lsp_ensure_capabilities{
		on_attach = on_attach,
		flags = {
			debounce_text_changes = 150,
		}
	})
end

lsp.pylsp.setup(coq.lsp_ensure_capabilities{
	cmd = { "pyls" },
	on_attach = on_attach,
	flags = {
		debounce_text_changes = 150,
	}
})

vim.lsp.handlers["textDocument/codeAction"] = require("lsputil.codeAction").code_action_handler
vim.lsp.handlers["textDocument/references"] = require("lsputil.locations").references_handler
vim.lsp.handlers["textDocument/definition"] = require("lsputil.locations").definition_handler
vim.lsp.handlers["textDocument/declaration"] = require("lsputil.locations").declaration_handler
vim.lsp.handlers["textDocument/typeDefinition"] = require("lsputil.locations").typeDefinition_handler
vim.lsp.handlers["textDocument/implementation"] = require("lsputil.locations").implementation_handler
vim.lsp.handlers["textDocument/documentSymbol"] = require("lsputil.symbols").document_handler
vim.lsp.handlers["workspace/symbol"] = require("lsputil.symbols").workspace_handler
