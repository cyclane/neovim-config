-- Variable settings
local set = vim.opt

vim.wo.wrap = false

set.guifont = "Fira Code Nerd Font:h10"
set.mouse = "a"
set.showcmd = true
set.number = true
set.relativenumber = true
set.tabstop = 4
set.shiftwidth = 0
set.encoding = "UTF-8"
set.spell = true
set.spelllang = "en_gb"
set.termguicolors = true

vim.o.clipboard = "unnamedplus"
vim.g.mapleader = " "

-- Plugin settings
-- General theming
vim.g.material_style = "deep ocean"
vim.cmd [[ colorscheme material ]]
vim.g.dashboard_default_executive = "telescope"
vim.g.dashboard_custom_shortcut = {
	last_session = "SPC S l",
	find_history = "SPC f h",
	find_file = "SPC f f",
	new_file = "SPC f n",
	change_colorscheme = "SPC T c",
	find_word = "SPC t w",
	book_marks = "SPC t m"
}

-- Code theming
set.list = true
set.listchars:append("space:⋅")
set.listchars:append("eol:↴")
vim.g.indentLine_fileTypeExclude = {"dashboard"}
require("indent_blankline").setup{
	show_end_of_line = true,
	space_char_blankline = " ",
	show_current_context = true,
	show_current_context_start = true
}
require("twilight").setup{
	context = 32
}
require("twilight").enable{}
require("colorizer").setup{ "*" }

-- Code helper extensions
require("nvim-treesitter.configs").setup{
	ensure_installed = "maintained",
	sync_install = false,
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false
	}
}
require("gitsigns").setup{
	keymaps = {}, -- managed by mappings.lua
	current_line_blame = true
}
require("neogit").setup{}
require("toggleterm").setup{}
function _G.set_terminal_keymaps()
	local opts = {noremap = true}
	vim.api.nvim_buf_set_keymap(0, "t", "<esc>", [[<C-\><C-n>]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-h>", [[<C-\><C-n><C-W>h]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-j>", [[<C-\><C-n><C-W>j]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-k>", [[<C-\><C-n><C-W>k]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-l>", [[<C-\><C-n><C-W>l]], opts)
end
vim.api.nvim_set_keymap("n", "<c-c>", "<cmd>exe v:count1 . \"ToggleTerm\"<CR>", { noremap = true })
vim.api.nvim_set_keymap("t", "<c-c>", "<esc><cmd>exe v:count1 . \"ToggleTerm\"<CR>", { noremap = true })
vim.cmd("autocmd! TermOpen term://* lua set_terminal_keymaps()")
require("nvim_comment").setup{}
vim.g.copilot_no_maps = true

-- UI modifiers and extensions
require("dressing").setup{}
require("notify").setup{}
require("nvim-web-devicons").setup{ default = true }
vim.g.nvim_tree_indent_markers = 1
vim.g.nvim_tree_git_hl = 1
vim.g.nvim_tree_highlight_opened_files = 1
require("nvim-tree").setup{}
require("lualine").setup{
	options = {
		theme = "material-nvim",
	},
	sections = {
		lualine_c = {"filename", "filesize"}
	},
	extensions = {"nvim-tree", "toggleterm"}
}
require("bufferline").setup{
	options = {
		numbers = "none",
		tab_size = 24,
		diagnostics = "nvim_lsp",
		diagnostics_indicator = function(count, level, diagnostics_dict, context)
			local icon = level:match("error") and " " or (level:match("warning") and " " or " ")
			return " " .. icon .. count .. " "
		end,
		offsets = {
			{filetype = "NvimTree"}
		}
	}
}

-- Coding QOL changes
require("mkdir")

-- Keymaps
require("winshift").setup{}

-- Language specific settings
vim.g.closetag_filetypes = "html,xhtml,phtml,xml,svelte"
