local wk = require("which-key")
local map = vim.api.nvim_set_keymap
wk.setup{}

local terminal = {
	name = "Terminal",
	t = { "<cmd>ToggleTermToggleAll<CR>", "Toggle" },
	["1"] = { "<cmd>ToggleTerm 1<CR>", "Toggle 1st" },
	["2"] = { "<cmd>ToggleTerm 2<CR>", "Toggle 2nd" },
	["3"] = { "<cmd>ToggleTerm 3<CR>", "Toggle 3rd" },
	["4"] = { "<cmd>ToggleTerm 4<CR>", "Toggle 4th" },
	["5"] = { "<cmd>ToggleTerm 5<CR>", "Toggle 5th" },
	["6"] = { "<cmd>ToggleTerm 6<CR>", "Toggle 6th" },
	["7"] = { "<cmd>ToggleTerm 7<CR>", "Toggle 7th" },
	["8"] = { "<cmd>ToggleTerm 8<CR>", "Toggle 8th" },
	["9"] = { "<cmd>ToggleTerm 9<CR>", "Toggle 9th" }
}


wk.register({
	["<leader>"] = { "<cmd>Telescope find_files<CR>", "Telescope fuzzy finder" },
	s = {
		name = "Search",
		g = { "<cmd>Telescope live_grep<CR>", "Live grep" },
		f = { "<cmd>Telescope current_buffer_fuzzy_find<CR>", "Fuzzy find" }
	},
	S = {
		name = "Session",
		s = { "<cmd>SessionSave<CR>", "Save" },
		l = { "<cmd>SessionLoad<CR>", "Load" }
	},
	L = {
		name = "Telescope",
		F = { "<cmd>Telescope fd<CR>", "fd" },
		L = { "<cmd>Telescope loclist<CR>", "Window location list" },
		M = { "<cmd>Telescope man_pages<CR>", "Man pages" },
		R = { "<cmd>Telescope reloader<CR>", "Lua module reloader" },
		S = { "<cmd>Telescope spell_suggest<CR>", "Spelling suggestions" },
		C = { "<cmd>Telescope commands<CR>", "Commands" },
		T = { "<cmd>Telescope treesitter<CR>", "Treesitter" },
		B = { "<cmd>Telescope builtin<CR>", "Builtin pickers" },

		t = { "<cmd>Telescope tags<CR>", "Tags" },
		r = { "<cmd>Telescope resume<CR>", "Resume" },
		p = { "<cmd>Telescope pickers<CR>", "Pickers" },
		P = { "<cmd>Telescope planets<CR>", "Planets" },
		q = { "<cmd>Telescope quickfix<CR>", "Quickfix" },
		m = { "<cmd>Telescope marks<CR>", "Marks" },
		c = { "<cmd>Telescope command_history<CR>", "Command history" },
		s = { "<cmd>Telescope search_history<CR>", "Search history" },
		j = { "<cmd>Telescope jumplist<CR>", "Jumplist" },
		h = { "<cmd>Telescope help_tags<CR>", "Help tags" },
		w = { "<cmd>Telescope live_grep<CR>", "Live grep" },

		G = {
			name = "Grep",
			l = { "<cmd>Telescope live_grep<CR>", "Live" },
			s = { "<cmd>Telescope grep_string<CR>", "String under cursor" }
		},
		v = {
			name = "ViM options and settings",
			T = { "<cmd>Telescope tagstack<CR>", "Tagstack" },
			t = { "<cmd>Telescope filetypes<CR>", "Types" },
			h = { "<cmd>Telescope highlights<CR>", "Highlights" },
			k = { "<cmd>Telescope keymaps<CR>", "Keymaps" },
			r = { "<cmd>Telescope registers<CR>", "Registers" },
			a = { "<cmd>Telescope autocommands<CR>", "Autocommands" },
			o = { "<cmd>Telescope vim_options<CR>", "Options" },
			c = { "<cmd>Telescope colorscheme<CR>", "Colourscheme" },
		},
		g = {
			name = "Git",
			s = { "<cmd>Telescope git_status<CR>", "Status" },
			f = { "<cmd>Telescope git_files<CR>", "Files" },
			c = { "<cmd>Telescope git_commits<CR>", "Commits" },
			C = { "<cmd>Telescope git_bcommits<CR>", "Buffer commits" },
			b = { "<cmd>Telescope git_branches<CR>", "Branches" },
			S = { "<cmd>Telescope git_stash<CR>", "Stash" }
		},
		f = {
			name = "Files",
			f = { "<cmd>Telescope find_files<CR>", "Find" },
			o = { "<cmd>Telescope oldfiles<CR>", "History" },
			b = { "<cmd>Telescope file_browser<CR>", "Browser" }
		},
		b = {
			name = "Current buffer",
			l = { "<cmd>Telescope buffers<CR>", "List" },
			f = { "<cmd>Telescope current_buffer_fuzzy_find<CR>", "Fuzzy find" },
			t = { "<cmd>Telescope current_buffer<tags<CR>", "Tags" },
			g = { "<cmd>Telescope git_bcommits<CR>", "Commits" },
			d = { "<cmd>Telescope diagnostics bufnr=0<CR>", "Diagnostics" },
			s = { "<cmd>Telescope lsp_document_symbols<CR>", "Document symbols" },
		},
		l = {
			name = "LSP",
			r = { "<cmd>Telescope lsp_references<CR>", "References" },
			d = { "<cmd>Telescope diagnostics<CR>", "Diagnostics" },
			s = { "<cmd>Telescope lsp_document_symbols<CR>", "Document symbols" },
			S = { "<cmd>Telescope lsp_workspace_symbols<CR>", "Workspace symbols" },
			w = { "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>", "Dynamic workspace symbols" },
			a = { "<cmd>Telescope lsp_code_actions<CR>", "Code actions" },
			A = { "<cmd>Telescope lsp_range_code_actions<CR>", "Code actions for range" },
			i = { "<cmd>Telescope lsp_implementations<CR>", "Implementations" },
			D = { "<cmd>Telescope lsp_definitions<CR>", "Definitions" },
			t = { "<cmd>Telescope lsp_type_definitions<CR>", "Type definitions" }
		}
	},
	v = {
		name = "ViM settings",
		T = { "<cmd>Telescope tagstack<CR>", "Tagstack" },
		t = { "<cmd>Telescope filetypes<CR>", "Types" },
		h = { "<cmd>Telescope highlights<CR>", "Highlights" },
		k = { "<cmd>Telescope keymaps<CR>", "Keymaps" },
		r = { "<cmd>Telescope registers<CR>", "Registers" },
		a = { "<cmd>Telescope autocommands<CR>", "Autocommands" },
		o = { "<cmd>Telescope vim_options<CR>", "Options" },
		c = { "<cmd>Telescope colorscheme<CR>", "Colourscheme" },
		w = { "<cmd>set wrap!<CR>", "Toggle wrap" },
		W = { "<cmd>Twilight<CR>", "Toggle twilight" }
	},
	f = {
		name = "File",
		n = { "<cmd>enew<CR>", "New" },
		s = { "<cmd>w<CR>", "Save" },
		h = { "<cmd>Telescope oldfiles<CR>", "History" },
		f = { "<cmd>Telescope find_files<CR>", "Find" },
		b = { "<cmd>Telescope file_browser<CR>", "Browser" },
		t = { "<cmd>NvimTreeToggle<CR>", "Tree" },
		T = { "<cmd>Telescope filetypes<CR>", "Types" },
		q = { "<cmd>q<CR>", "Quit" }
	},
	t = terminal,
	p = {
		name = "Packer",
		c = { "<cmd>PackerClean<CR>", "Clean" },
		C = { "<cmd>PackerCompile<CR>", "Compile" },
		i = { "<cmd>PackerInstall<CR>", "Install" },
		s = { "<cmd>PackerStatus<CR>", "Status" },
		S = { "<cmd>PackerSync<CR>", "Sync" },
		u = { "<cmd>PackerUpdate<CR>", "Update" }
	},
	g = {
		name = "Git",
		n = { "<cmd>Neogit<CR>", "Neogit" },
		s = { "<cmd>Telescope git_status<CR>", "Status" },
		f = { "<cmd>Telescope git_files<CR>", "Files" },
		c = { "<cmd>Telescope git_commits<CR>", "Commits" },
		C = { "<cmd>Telescope git_bcommits<CR>", "Buffer commits" },
		b = { "<cmd>Telescope git_branches<CR>", "Branches" },
		S = { "<cmd>Telescope git_stash<CR>", "Stash" }
	},
	b = {
		name = "Buffer",
		c = { "<cmd>Bdelete<CR>", "Close" },
		l = { "<cmd>Telescope buffers<CR>", "List" },
		f = { "<cmd>Telescope current_buffer_fuzzy_find<CR>", "Fuzzy find" },
		t = { "<cmd>Telescope current_buffer<tags<CR>", "Tags" },
		g = { "<cmd>Telescope git_bcommits<CR>", "Commits" },
		d = { "<cmd>Telescope diagnostics bufnr=0<CR>", "Diagnostics" },
		s = { "<cmd>Telescope lsp_document_symbols<CR>", "Document symbols" },
		n = { "<cmd>bn<CR>", "Next buffer" },
		p = { "<cmd>bp<CR>", "Previous buffer" },
		C = { "<cmd>bd<CR>", "Close with window" }
	},
	w = { "<c-w>", "Window" }
}, { prefix = "<leader>" })

wk.register({
	["<c-m>"] = { "<cmd>WinShift<CR>", "WinShift" },
	m = { "<cmd>WinShift<CR>", "WinShift" },
	X = { "<cmd>WinShift swap<CR>", "WinShift swap" }
}, { prefix = "<c-w>" })

local opts = { noremap = true }

vim.api.nvim_set_keymap("n", "<c-_>", ":CommentToggle<CR>", opts)
vim.api.nvim_set_keymap("v", "<c-_>", ":CommentToggle<CR>", opts)

vim.api.nvim_set_keymap("n", "<c-t>", "<cmd>ToggleTermToggleAll<CR>", opts)
vim.api.nvim_set_keymap("t", "<c-t>", "<cmd>ToggleTermToggleAll<CR>", opts)
vim.api.nvim_set_keymap("i", "<c-t>", "<cmd>ToggleTermToggleAll<CR>", opts)

-- vim.api.nvim_set_keymap("i", "<c-a>", "<cmd>call copilot#Accept()<CR>", {})
-- vim.api.nvim_set_keymap("i", "<c-d>", "<cmd>call copilot#Dismiss()<CR>", {})
vim.cmd [[
	imap <script><silent><nowait><expr> <C-a> copilot#Accept()
	imap <silent><script><nowait><expr> <C-d> copilot#Dismiss()
]]
