return require("packer").startup(function(use)
	-- Let Packer manage itself
	use "wbthomason/packer.nvim"

	-- LSP and autocompletion
	use "neovim/nvim-lspconfig"
	use { "RishabhRD/nvim-lsputils", requires = "RishabhRD/popfix" }
	use { "ms-jpq/coq_nvim", branch = "coq"	}
	use { "ms-jpq/coq.artifacts", branch = "artifacts" }
	use { "ms-jpq/coq.thirdparty", branch = "3p" }

	-- General theming
	use "marko-cerovac/material.nvim"
	use "glepnir/dashboard-nvim"
	use "dstein64/nvim-scrollview"

	-- Code theming
	use "lukas-reineke/indent-blankline.nvim"
	use "folke/twilight.nvim"
	use "norcalli/nvim-colorizer.lua"
	use "yamatsum/nvim-cursorline"

	-- Code helper extensions
	use { "nvim-treesitter/nvim-treesitter", run = "<cmd>TSUpdate" }
	use	{ "nvim-telescope/telescope.nvim", requires = "nvim-lua/plenary.nvim" }
	use { "lewis6991/gitsigns.nvim", requires = "nvim-lua/plenary.nvim" }
	use { "TimUntersberger/neogit", requires = "nvim-lua/plenary.nvim" }
	use "akinsho/toggleterm.nvim"
	use "tversteeg/registers.nvim"
	use "terrortylor/nvim-comment"
	use "editorconfig/editorconfig-vim"
	use "github/copilot.vim"

	-- ViM helper extensions
	use "famiu/bufdelete.nvim"

	-- UI modifiers and extensions
	use "stevearc/dressing.nvim"
	use "rcarriga/nvim-notify"
	use { "kyazdani42/nvim-tree.lua", requires = "kyazdani42/nvim-web-devicons" }
	use { "nvim-lualine/lualine.nvim", requires = "kyazdani42/nvim-web-devicons" }
	use { "akinsho/bufferline.nvim", requires = "kyazdani42/nvim-web-devicons" }

	-- Coding QOL changes
	use "jghauser/mkdir.nvim"
	use "jiangmiao/auto-pairs"

	-- Keymaps
	use "folke/which-key.nvim"
	use "sindrets/winshift.nvim"

	-- Language specific extensions
	use "evanleck/vim-svelte"
	use "alvan/vim-closetag"
end)
