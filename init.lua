-- Plugins
require("plugins")

-- Vim settings
require("settings")

-- LSP Config
require("lsp")

-- Mappings
require("mappings")
